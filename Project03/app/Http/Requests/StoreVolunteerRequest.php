<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVolunteerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'city' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:volunteers,email'],
            'phone_number' => ['required', 'string'],
            'volunteer_position_id' => ['required', 'integer'],
            'details' => ['required', 'string', 'max:1000'],
            'doc1' => ['mimes:pdf,docx'],
        ];
    }
}
